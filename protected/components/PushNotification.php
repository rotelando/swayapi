<?php
/**
 * Created by PhpStorm.
 * User: RotelandO
 * Date: 10/25/14
 * Time: 9:18 PM
 */


class PushNotification
{


    public $gcm_id = array();

    public $message = array();

    public $headers;

    public function __construct($gcm_id, $message)
    {

        $this->gcm_id = array($gcm_id);

        $this->message = $message;

        $this->headers = array
        (
            'Authorization: key=' . Yii::app()->params['API_ACCESS_KEY'],
            'Content-Type: application/json'
        );
    }

    public function doPush()
    {

        $fields = array
        (
            'registration_ids' => $this->gcm_id,
            'data' => $this->message
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, Yii::app()->params['GOOGLE_NOTIFICATION_URL'] );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $this->headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        return $result;
    }
}