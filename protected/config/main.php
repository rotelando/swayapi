<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'AppName'=>'Sway App',
        //'host' => array('hosts' => array('http://ec2-54-186-106-88.us-west-2.compute.amazonaws.com:9200')),
        'host' => array('hosts' => array('http://sway.intelworx.com:9200')),
        'API_ACCESS_KEY' => 'AIzaSyAdZsATU8ArfV6QDGNvXr8N4qOKBpc4nqU',
        'GOOGLE_NOTIFICATION_URL' => 'https://android.googleapis.com/gcm/send',
        //'GCM_ID' => 'APA91bHwaHq7Bf0Ud25P5MDdQyH5Ls3HC2lduuR0-J82k3muj0iApXGNWyi6nIuDgRTGNT3gdYEk065S3UXDBZkZ0b3WcrDWH_SpS1wN7zrn4Y-eeqoBzPjK_7uEmo3oebS-dIAvb7ywxhNJSByienCMp7QF0Gilpw'
        'GCM_ID' => 'APA91bH4y9Tq_56kp6eV7O2XgFFPQ2r5D9w1FAtSIdfdWVSx1QO-q1fNo08UXkOzV88-RblPxI3Gahya6y0aBY3JbhRPx6xm9ZZkanIRxShAslEuAjstt7E87f20gTdLiiF1PV7Aa4OZ2bBy0R0amFQqkCtJiNMlGA'
    ),

	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Sway App',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'password',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),

	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
                '' => 'tweets/welcome',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

        /*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		// uncomment the following to use a MySQL database
		*/
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=swaydb',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'sway123',
			'charset' => 'utf8',
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),
);