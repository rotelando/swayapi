/*
 Navicat Premium Data Transfer

 Source Server         : Local.MySQL
 Source Server Type    : MySQL
 Source Server Version : 50617
 Source Host           : rotelando.local
 Source Database       : swaydb

 Target Server Type    : MySQL
 Target Server Version : 50617
 File Encoding         : utf-8

 Date: 10/26/2014 02:24:37 AM
*/
SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `pushed_data`
-- ----------------------------
DROP TABLE IF EXISTS `pushed_data`;
CREATE TABLE `pushed_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gcm_id` varchar(255) NOT NULL,
  `param_data` varchar(255) NOT NULL,
  `response_status` int(11) NOT NULL,
  `response` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `pushed_data`
-- ----------------------------
BEGIN;
INSERT INTO `pushed_data` VALUES ('1', 'APA91bHwaHq7Bf0Ud25P5MDdQyH5Ls3HC2lduuR0-J82k3muj0iApXGNWyi6nIuDgRTGNT3gdYEk065S3UXDBZkZ0b3WcrDWH_SpS1wN7zrn4Y-eeqoBzPjK_7uEmo3oebS-dIAvb7ywxhNJSByienCMp7QF0Gilpw', '{\n	\"data\" : {\n		\"roads\" : [ \n        	\'Yaba\', \'sabo\' ],\n		\"gcm_id\" : \"APA91bHwaHq7Bf0Ud25P5MDdQyH5Ls3HC2lduuR0-J82k3muj0iApXGNWyi6nIuDgRTGNT3gdYEk065S3UXDBZkZ0b3WcrDWH_SpS1wN7zrn4Y-eeqoBzPjK_7uEmo3oebS-dIAvb7ywxhNJSByienCMp7QF0Gilpw\",\n		\"est_time\" : 343.3', '400', '{\"_index\":\"river\",\"_type\":\"status\",\"_id\":\"526123907515170816\",\"_score\":null,\"_source\":{\"id\":526123907515170816,\"text\":\"RT @TosynBucknor: 5pm at Ember Creek, 32 Awolowo Road,  Ikoyi, Lagos.\\nMusic! Magic! One Mic Naija\\n#OneMicNaijaGreenGreenGreen http:\\/\\', '2014-10-26 01:43:53'), ('2', 'APA91bHwaHq7Bf0Ud25P5MDdQyH5Ls3HC2lduuR0-J82k3muj0iApXGNWyi6nIuDgRTGNT3gdYEk065S3UXDBZkZ0b3WcrDWH_SpS1wN7zrn4Y-eeqoBzPjK_7uEmo3oebS-dIAvb7ywxhNJSByienCMp7QF0Gilpw', '{\n	\"data\" : {\n		\"roads\" : [ \n        	\'Yaba\', \'sabo\' ],\n		\"gcm_id\" : \"APA91bHwaHq7Bf0Ud25P5MDdQyH5Ls3HC2lduuR0-J82k3muj0iApXGNWyi6nIuDgRTGNT3gdYEk065S3UXDBZkZ0b3WcrDWH_SpS1wN7zrn4Y-eeqoBzPjK_7uEmo3oebS-dIAvb7ywxhNJSByienCMp7QF0Gilpw\",\n		\"est_time\" : 343.3', '200', '{\"took\":26,\"timed_out\":false,\"_shards\":{\"total\":5,\"successful\":5,\"failed\":0},\"hits\":{\"total\":1,\"max_score\":null,\"hits\":[{\"_index\":\"river\",\"_type\":\"status\",\"_id\":\"526123907515170816\",\"_score\":null,\"_source\":{\"id\":526123907515170816,\"text\":\"RT @TosynBucknor', '2014-10-26 02:00:35'), ('3', 'APA91bHwaHq7Bf0Ud25P5MDdQyH5Ls3HC2lduuR0-J82k3muj0iApXGNWyi6nIuDgRTGNT3gdYEk065S3UXDBZkZ0b3WcrDWH_SpS1wN7zrn4Y-eeqoBzPjK_7uEmo3oebS-dIAvb7ywxhNJSByienCMp7QF0Gilpw', '{\n	\"data\" : {\n		\"roads\" : [ \n        	\'awolowo way\', \'johnson\' ],\n		\"gcm_id\" : \"APA91bHwaHq7Bf0Ud25P5MDdQyH5Ls3HC2lduuR0-J82k3muj0iApXGNWyi6nIuDgRTGNT3gdYEk065S3UXDBZkZ0b3WcrDWH_SpS1wN7zrn4Y-eeqoBzPjK_7uEmo3oebS-dIAvb7ywxhNJSByienCMp7QF0Gilpw\",\n		\"est_tim', '200', '{\"took\":20,\"timed_out\":false,\"_shards\":{\"total\":5,\"successful\":5,\"failed\":0},\"hits\":{\"total\":0,\"max_score\":null,\"hits\":[]}}', '2014-10-26 02:11:18'), ('4', 'APA91bHwaHq7Bf0Ud25P5MDdQyH5Ls3HC2lduuR0-J82k3muj0iApXGNWyi6nIuDgRTGNT3gdYEk065S3UXDBZkZ0b3WcrDWH_SpS1wN7zrn4Y-eeqoBzPjK_7uEmo3oebS-dIAvb7ywxhNJSByienCMp7QF0Gilpw', '{\n	\"data\" : {\n		\"roads\" : [ \n        	\'awolowo way\', \'johnson\' ],\n		\"gcm_id\" : \"APA91bHwaHq7Bf0Ud25P5MDdQyH5Ls3HC2lduuR0-J82k3muj0iApXGNWyi6nIuDgRTGNT3gdYEk065S3UXDBZkZ0b3WcrDWH_SpS1wN7zrn4Y-eeqoBzPjK_7uEmo3oebS-dIAvb7ywxhNJSByienCMp7QF0Gilpw\",\n		\"est_tim', '200', '{\"took\":6,\"timed_out\":false,\"_shards\":{\"total\":5,\"successful\":5,\"failed\":0},\"hits\":{\"total\":112,\"max_score\":null,\"hits\":[{\"_index\":\"river\",\"_type\":\"status\",\"_id\":\"526179223720194048\",\"_score\":null,\"_source\":{\"id\":526179223720194048,\"text\":\"Easy to say thi', '2014-10-26 02:11:30'), ('5', 'APA91bHwaHq7Bf0Ud25P5MDdQyH5Ls3HC2lduuR0-J82k3muj0iApXGNWyi6nIuDgRTGNT3gdYEk065S3UXDBZkZ0b3WcrDWH_SpS1wN7zrn4Y-eeqoBzPjK_7uEmo3oebS-dIAvb7ywxhNJSByienCMp7QF0Gilpw', '{\n	\"data\" : {\n		\"roads\" : [ \n        	\'awolowo way\', \'johnson\' ],\n		\"gcm_id\" : \"APA91bHwaHq7Bf0Ud25P5MDdQyH5Ls3HC2lduuR0-J82k3muj0iApXGNWyi6nIuDgRTGNT3gdYEk065S3UXDBZkZ0b3WcrDWH_SpS1wN7zrn4Y-eeqoBzPjK_7uEmo3oebS-dIAvb7ywxhNJSByienCMp7QF0Gilpw\",\n		\"est_tim', '200', '{\"took\":141,\"timed_out\":false,\"_shards\":{\"total\":5,\"successful\":5,\"failed\":0},\"hits\":{\"total\":12330,\"max_score\":null,\"hits\":[{\"_index\":\"river\",\"_type\":\"status\",\"_id\":\"526179314002186240\",\"_score\":null,\"_source\":{\"id\":526179314002186240,\"text\":\"While in tr', '2014-10-26 02:11:45'), ('6', 'Push', '\n{\"took\":4,\"timed_out\":false,\"_shards\":{\"total\":5,\"successful\":5,\"failed\":0},\"hits\":{\"total\":123720,\"max_score\":1.0,\"hits\":[{\"_index\":\"river\",\"_type\":\"status\",\"_id\":\"526120951390273536\",\"_score\":1.0,\"_source\":{\"id\":526120951390273536,\"text\":\"#slimz Wenger', '200', '{\"_index\":\"river\",\"_type\":\"status\",\"_id\":\"526120951390273536\",\"_score\":1,\"_source\":{\"id\":526120951390273536,\"text\":\"#slimz Wenger left in awe of lightning-fast Alexis: The Chilean scored both goals as Arsenal recorded a 2-0 wi... http:\\/\\/t.co\\/etWMahGsBV', '2014-10-26 02:21:58');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
