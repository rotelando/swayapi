<?php

/**
 * Created by PhpStorm.
 * User: RotelandO
 * Date: 9/15/14
 * Time: 3:24 PM
 */
class TweetsController extends Controller {

    public $defaultAction = 'welcome';
    public $response = array();

    const DEVICE_ID_TYPE_PHONE = 'PHONE';
    const DEVICE_ID_TYPE_GCM = 'GCM';

    public $stripped_words = array(
        'way',
        'road',
        'street',
        'lane',
        'junction'
    );
    private $esClient;

    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
        $host_params = Yii::app()->params['host'];
        $this->esClient = new Elasticsearch\Client($host_params);
    }

    public function actionWelcome() {

        header('Content-Type: application/json');

        $json_output['meta']["status"] = 200;
        $json_output['meta']["message"] = "Nothing so see here. Move along.";

        $json_output['data']["name"] = "Sway API";
        $json_output['data']["version"] = '1.0-sway';
        $json_output['data']["company"] = 'Alumni OAU';
        $json_output['data']["author"] = "Ask me???";
        $json_output['data']["date"] = "Thursday 25, October 2014";

        $this->renderPartial('_ajaxResponse', array('response' => json_encode($json_output)));
    }

    public function actionPushTweet() {

        $body = file_get_contents('php://input');

        $arrBody = CJSON::decode($body);
        if (!empty($arrBody)) {
            foreach ($this->getWaitingDevices() as $aDevice) {
                $roads = explode(',', $aDevice['roads']);
                $limit = $aDevice['device_id_type'] === self::DEVICE_ID_TYPE_GCM ? 5 : 1;
                $results = $this->_doSearch($roads, $limit, $aDevice['max_id']);
                //set max ID
                if (!empty($results)) {
                    if ($aDevice['device_id_type'] === self::DEVICE_ID_TYPE_GCM) {
                        if ($this->notifyGCM($aDevice['device_id'], $results)) {
                            Yii::log("GCM Published: " . $aDevice['device_id']);
                        }
                    } else {
                        //send sms
                        if (SMSSender::send($aDevice['device_id'], $results[0]['text'])) {
                            //sent
                            Yii::log("SMS sent to: " . $aDevice['device_id']);
                        }
                    }

                    $this->updateMaxId($aDevice['id'], $results[0]['_id']);
                }
            }
        }

        //
        $this->echoJSONResponse(200, 'OK');
    }

    private function updateMaxId($sessionId, $nexMaxId) {
        $session = Session::model()->findByPk($sessionId);
        if ($session) {
            $session->max_id = $nexMaxId;
            return $session->save();
        }
        return null;
    }

    private function notifyGCM($gcmId, $results) {
        $msg = array
            (
            'message' => $results,
            'title' => 'SwayTraffic Smart'
        );

        $pushClient = new PushNotification($gcmId, $msg);

        $resp = $pushClient->doPush();
        error_log(json_encode($resp));
        return true;
    }

    private function getWaitingDevices() {
        return TweetModel::pullTimedUser();
    }

    private function minimizeTweet($hit, $docId) {
        $temp = [];
        $temp['text'] = $hit['text'];
        $temp['user'] = $hit['user'];
        $temp['user']['id_str'] = strval($hit['user']['id']);
        $temp['_id'] = $docId;
        $temp['created_at'] = $hit['created_at'];
        return $temp;
    }

    private function minimizeTweets($arr, $max = null) {
        $msgArray = array();
        foreach ($arr['hits']['hits'] as $hit) {
            if (!$max || $hit['_id'] > $max) {
                $msgArray[] = $this->minimizeTweet($hit['_source'], $hit['_id']);
            }
        }
        return $msgArray;
    }

    /* {
      "data" : {
      "roads" : [ 'Yaba', 'Sabo', 'Bariga' ],
      "gcm_id" : "jdu733bdbvakd3218bgadsavasd",
      "est_time" : 343.32
      }
      } */

    private function _doSearch($roads, $size = 20, $maxId = null) {
        $searchParams = [];
        $searchParams['index'] = 'sway';
        $searchParams['type'] = 'tweets';

        //Stripped the common words from the road array
        $stripped_roads = $this->stripWordsFromRoad($roads);
        $wordArray = array();
        foreach ($stripped_roads as $road) {
            $parts = preg_split('/[^a-zA-Z0-9]+/', $road);
            foreach ($parts as $part) {
                if (strlen($part) > 2 && !in_array($part, $wordArray)) {
                    $wordArray[] = $part;
                }
            }
        }


        if (empty($wordArray)) {
            return [];
        }


        $searchParams['body']['query']['query_string'] = array(
            'default_field' => 'text',
            'query' => join(' OR ', $wordArray)
        );

//        if ($maxId) {
//            $searchParams['body']['query']['range']['id']['gte'] = $maxId;
//        }
        //$searchParams['body']['query']['bool']['minimum_should_match'] = 1;
        $searchParams['body']['sort']['created_at']['order'] = ['desc'];
        $searchParams['body']['size'] = $size;
        return $this->minimizeTweets($this->esClient->search($searchParams), $maxId);
    }

    private function _doRoadQuery($roads, $sessionId) {
        try {
            //save data in db
            $results = $this->_doSearch($roads);

            if (!empty($results)) {
                $this->updateMaxId($sessionId, $results[0]['_id']);
            }

            $this->echoJSONResponse(200, "Request Successful!", $this->_doSearch($roads));
        } catch (Exception $e) {

            $this->echoJSONResponse(500, $e->getMessage());
        }
    }

    private function createSession($estTime, $deviceId, $deviceIdType, $roads) {
        $session = new Session;
        $session->duration = $estTime;
        $session->device_id = $deviceId;
        $session->device_id_type = $deviceIdType;
        //$session->roads = $roads;
        if ($session->save()) {
            //$roads = array_unique($roads);
            $roadsStripped = $this->stripWordsFromRoad($roads);
            for ($i = 0; $i < count($roads); $i++) {
                $session_road = new SessionRoad;
                $session_road->session_id = $session->id;
                $session_road->road = $roads[$i];
                $session_road->road_stripped = $roadsStripped[$i];
                $session_road->save();
            }
            return $session->id;
        } else {
            return null;
        }
    }

    public function actionByRoad() {
        $request = Yii::app()->request;
        if (!$request->isPostRequest) {
            $this->echoJSONResponse(400, "Bad Request!");
        }

        $body = file_get_contents('php://input');
        $queryParams = CJSON::decode($body);
        $gcm_id = isset($queryParams['data']['gcm_id']) ? $queryParams['data']['gcm_id'] : null;

        $roads = isset($queryParams['data']['roads']) ? $queryParams['data']['roads'] : null;


        if (is_null($gcm_id) || is_null($roads)) {
            $this->echoJSONResponse(400, "Bad Request!");
        }

        if (isset($queryParams['data']['est_time'])) {
            $est_tme = $queryParams['data']['est_time'];
        }

        $sessionId = $this->createSession($est_tme, $gcm_id, self::DEVICE_ID_TYPE_GCM, $roads);
        $this->_doRoadQuery($roads, $sessionId);
    }

    public function actionSMS() {

        $request = Yii::app()->request;

        $message = $request->getPost('message');

        $arrString = json_decode($message, true);

        $phone = trim($arrString['phone_number'], '+');
        $estTime = $arrString['est_time'];
        $roads = $arrString['roads'];

        if (is_null($phone) || empty($roads)) {
            $this->echoJSONResponse(400, "Bad Request!");
        }

        $sessionId = $this->createSession($estTime, $phone, self::DEVICE_ID_TYPE_PHONE, $roads);
        $res = $this->_doSearch($roads, 1);
        if (!empty($res)) {
            $this->updateMaxId($sessionId, $res[0]['_id']);
            if (SMSSender::send($phone, $res[0]['text'])) {
                $this->echoJSONResponse(200, "Request Successful!");
            }
        }


        $this->echoJSONResponse(400, "Bad Request!");
    }

    private function stripWordsFromRoad($roads) {

        $result = array();

        foreach ($roads as $road) {

            $lroad = strtolower($road);

            foreach ($this->stripped_words as $word) {

                if (strpos($lroad, $word) == -1)
                    continue;

                $lroad = str_replace($word, '', $lroad);
            }

            $result[] = trim($lroad);
        }

        return $result;
    }

}
