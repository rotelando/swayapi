<?php
/**
 * Created by PhpStorm.
 * User: RotelandO
 * Date: 10/26/14
 * Time: 1:28 AM
 */

class TweetModel extends CActiveRecord {



    public static function saveTweet($data) {

        $connection = Yii::app()->db;

        $sql = "INSERT INTO pushed_data (gcm_id, param_data, response_status, response, created_at)
                VALUES (:gcm_id, :param_data, :response_status, :response, :created_at)";

        $command = $connection->createCommand($sql);

        $command->bindParam(':gcm_id', $data['gcm_id'], PDO::PARAM_STR);
        $command->bindParam(':param_data', $data['param_data'], PDO::PARAM_STR);
        $command->bindParam(':response_status', $data['response_status'], PDO::PARAM_STR);
        $command->bindParam(':response', $data['response'], PDO::PARAM_STR);
        $command->bindParam(':created_at', $data['created_at'], PDO::PARAM_STR);

        $result = $command->execute();

        return $result;
    }

    public static function pullTimedUser() {

        $connection = Yii::app()->db;

        $sql = "SELECT `device_id`, `device_id_type`, GROUP_CONCAT(sr.`road_stripped`) AS roads, s.`max_id`, s.id
                FROM `session` s
                INNER JOIN session_road sr ON sr.`session_id` = s.`id`
                WHERE s.`is_active`
                AND (UNIX_TIMESTAMP(s.`start_time`) + `duration`) > UNIX_TIMESTAMP() GROUP BY s.id";

        $command = $connection->createCommand($sql);

        $result = $command->queryAll();

        return $result;
    }
} 