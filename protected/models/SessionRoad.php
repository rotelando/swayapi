<?php
/**
 * Created by PhpStorm.
 * User: RotelandO
 * Date: 10/26/14
 * Time: 9:13 AM
 */

class SessionRoad extends CActiveRecord {

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'session_road';
    }

} 