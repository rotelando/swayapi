<?php
/**
 * Created by PhpStorm.
 * User: RotelandO
 * Date: 10/26/14
 * Time: 8:44 AM
 */

class SessionModel extends CActiveRecord {

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'session';
    }
} 