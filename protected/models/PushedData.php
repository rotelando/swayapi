<?php
/**
 * Created by PhpStorm.
 * User: RotelandO
 * Date: 10/26/14
 * Time: 9:25 AM
 */

class PushedData extends CActiveRecord {

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'pushed_data';
    }
} 